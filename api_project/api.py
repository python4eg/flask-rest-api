from flask import Flask, Blueprint
from flask_restx import Api


app = Flask(__name__)
bp = Blueprint('api_v1', 'api', url_prefix='/v1')
api = Api(app=bp, title='API V1')


