from dataclasses import dataclass, asdict
from http import HTTPStatus

from pydantic import BaseModel
from flask_restx import Resource, fields, abort

from api_project.api import api
from api_project.views.gadget import gadget_ns

GadgetModel = gadget_ns.model('Gadget', {
    'id': fields.Integer(description='Gadget id', required=True),
    'name': fields.String(description='Gadget name'),
    'image': fields.String(description='Gadget photo')
})

@dataclass
class GadgetDataclass:
    id: int
    name: str
    image: str = None


class GadgetPydantic(BaseModel):
    id: int
    name: str
    image: str = None

data = [
            asdict(GadgetDataclass(id=1, name='Name 1')),
            asdict(GadgetDataclass(id=2, name='Name 2')),
            asdict(GadgetDataclass(id=3, name='Name 3', image='name.png')),
            asdict(GadgetDataclass(id=None, name='Name 4')),
            GadgetPydantic(id=1, name='Name 4'),
            {'id': 10, 'name': 'Name 1'},
            {'id': 20, 'name': 'Name 2', 'extra_field': 100},
            {'id': 30, 'name': 'Name 3', 'photo': 'name.png'},
            {'id': 40, 'name': 'Name 4'},
        ]

@gadget_ns.route('/', methods=['GET', 'POST'])
class GadgetViews(Resource):
    @gadget_ns.marshal_list_with(GadgetModel)
    def get(self):
        return data

    @gadget_ns.expect(GadgetModel)
    def post(self):
        if api.payload.get('id') is None:
            abort(code=HTTPStatus.BAD_REQUEST, message='Id cannot be None')
        data.append(api.payload)
