from flask import Flask

from api_project.api import app, api, bp
from api_project.views.gadget.views import gadget_ns

api.add_namespace(gadget_ns, path=gadget_ns.path)
app.register_blueprint(bp)

if __name__ == '__main__':
    app.run()
